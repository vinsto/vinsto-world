<?php

namespace Vinsto\Agency\Agent;


use GuzzleHttp\Client;
use Npaf\Authentication\AccessTokenMetadata;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Yaml\Yaml;

class RemoteServiceAgent
{
//    const POD_INFO_REPORTER_c41a5d9__Prj_name = 52684;
    const VINSTO_WORLD_CONFIG_INFO_CENTER_YAML = __DIR__ . '/../../../../000_config/000_info_center.yaml';

    const VINSTO_REMOTE_SERVICE_MANAGEMENT_URL___SERVICE_NAME = 'vrs--remote-service-management-root';

    private $pir;
    /**
     * @var Request
     */
    private $request;

    /** @var array $infoCenterConfig */
    private $infoCenterConfig;

    /** @var array $config */
    private $config;

    /** @var string infoCenterURL */
    private $infoCenterURL;

    /** @var string $authCenterURL */
    private $authCenterURL;

    /** @var string $infoCenterParsingRemoteServiceApi */
    private $infoCenterParsingRemoteServiceApi;

    public function __construct(RequestStack $requestStack=null)
    {
        list($this->infoCenterConfig, $this->infoCenterURL, $this->config)
                = $this->fetchInfoCenterAndBaseConfig(self::VINSTO_WORLD_CONFIG_INFO_CENTER_YAML);

        $this->oauth2CenterURL =
            $this->config['global']['OAUTH2_CENTER_bbecd26__Protocal'] . '://' .
            $this->config['global']['OAUTH2_CENTER_bbecd26__Host'] . ':' .
            $this->config['global']['OAUTH2_CENTER_bbecd26__Port'];

        $this->authCenterURL =
            $this->config['global']['AUTHENTICATION_CENTER_df20c3b__Protocal'] . '://' .
            $this->config['global']['AUTHENTICATION_CENTER_df20c3b__Host'] . ':' .
            $this->config['global']['AUTHENTICATION_CENTER_df20c3b__Port'];

        $this->infoCenterParsingRemoteServiceApi =
            $this->infoCenterConfig['global']['INFO_CENTER_a681207__parsing_Remote_service_Api'];

        $url = $this->infoCenterURL . $this->infoCenterParsingRemoteServiceApi;
        $client = new Client();
        $response = $client->get("$url/" . self::VINSTO_REMOTE_SERVICE_MANAGEMENT_URL___SERVICE_NAME);
        /**
         * remote service management 的 URL 不能再用 $agent->parse()，否则死循环
         */
        $this->remoteServiceManagementURL = (string) $response->getBody();
    }

    public function fetchInfoCenterAndBaseConfig($infoCenterConfPath)
    {
        $infoCenterConfig = Yaml::parse(file_get_contents($infoCenterConfPath));
        $infoCenterURL =
            $infoCenterConfig['global']['INFO_CENTER_a681207__Protocal'] . '://' .
            $infoCenterConfig['global']['INFO_CENTER_a681207__Host'] . ':' .
            $infoCenterConfig['global']['INFO_CENTER_a681207__Port'];

        $client = new Client();
        $response = $client->get("$infoCenterURL/api/base-yaml");
        $baseYamlRaw = (string) $response->getBody();
        $config = Yaml::parse($baseYamlRaw);

        return [$infoCenterConfig, $infoCenterURL, $config];
    }

    protected function getStandardSchemeAndHttpHost(): string
    {
        $protocol = $_SERVER['REQUEST_SCHEME'];
        $host = $_SERVER['HTTP_HOST'];
        $port = $_SERVER['SERVER_PORT'];

        return "$protocol://$host:$port";
    }

    public function parse(string $serviceName, array $config)
    {
//        $host = $this->getHost();
        $json = json_encode($config);
//        $url = "$host/$this->pir/parse-service/$serviceName";
        $url = "$this->remoteServiceManagementURL/parse-service/$serviceName";
        $client = new Client();
        $response = $client->post($url, [
            'form_params' => [
                'json' => $json,
            ]
        ]);

        $result = (string) $response->getBody();

        return $result;
    }


    public function checkAccessToken($accessToken)//check OAuth2 Token
    {
        $fb = new \Npaf\Npaf([
            'app_id' =>  $this->config['global']['OAUTH2_CENTER_bbecd26__OAuth2_app_id'],
            'app_secret' => $this->config['global']['OAUTH2_CENTER_bbecd26__OAuth2_app_secret'],
            'default_woar_version' => 'v'. \Npaf\Npaf::DEFAULT_WOAR_VERSION,
            'oauth2_url' => $this->oauth2CenterURL,
            'woar_url' => $this->oauth2CenterURL . '/woar',
        ]);

        $oAuth2Client = $fb->getOAuth2Client();
        $tokenMetadata = $oAuth2Client->debugToken($accessToken);
        if ($tokenMetadata instanceof AccessTokenMetadata) {
            return true;
        }

        return false;
    }

    public function checkJWTToken($token)
    {
        $parts = explode('.', $token);
        $head = $parts[0];
        $body = $parts[1];
        $content = json_decode(base64_decode($body));
        $signature = $parts[2];
        $message = $head . '.' . $body;
        $secret = $this->config['global']['AUTHENTICATION_CENTER_df20c3b__System_Secret'];

        $signiture2 = hash_hmac("sha256", $message, $secret);

        if ($signature === $signiture2 ){

            return true;

        }else{

            return false;
        }
    }

    public function getOauth2CenterURL()
    {
        return $this->oauth2CenterURL;
    }

    public function getAuthCenterURL()
    {
        return $this->authCenterURL;
    }

    public function getOauh2LoginURL($success__redirect_url)
    {
        $fb = new \Npaf\Npaf([
            'app_id' => $this->config['global']['OAUTH2_CENTER_bbecd26__OAuth2_app_id'],
            'app_secret' => $this->config['global']['OAUTH2_CENTER_bbecd26__OAuth2_app_secret'],
            'default_woar_version' => 'v'. \Npaf\Npaf::DEFAULT_WOAR_VERSION,
            'oauth2_url' => $this->oauth2CenterURL,
            'woar_url' => $this->oauth2CenterURL . '/woar',
        ]);

        $helper = $fb->getRedirectLoginHelper();
        $permissions = ['email']; // Optional permissions
//        $success__redirect_url = $_SERVER['REQUEST_SCHEME']. '://' . $_SERVER['HTTP_HOST'] . '/npaf-callback.php';
        $loginUrl = $helper->getLoginUrl($success__redirect_url, $permissions);

        return $loginUrl;
    }

    public function getJWTAuthLoginURL($success__redirect_url) {
        $url = $this->authCenterURL . '/login?need_jwt=1&redirect_url=' . urlencode($success__redirect_url);

        return $url;
    }
}
