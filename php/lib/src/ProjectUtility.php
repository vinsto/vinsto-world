<?php

namespace Vinsto\Lib;

class ProjectUtility
{
    public static function tr4Shell(string $name): array
    {
        $newName = str_replace('-', '_', $name);

        switch ($newName) {
//            case 'remote_service_management':
//                $systemName = 'REMOTE_SERVICE_MANAGEMENT_ce78b80';
//                break;
            case 'authentication_center':
                $systemName = 'AUTHENTICATION_CENTER_df20c3b';
                break;
            case 'oauth2_center':
                $systemName = 'OAUTH2_CENTER_bbecd26';
                break;
            case 'info_center':
                $systemName = 'INFO_CENTER_a681207';
                break;
            case 'pod_info_reporter':
                $systemName = 'POD_INFO_REPORTER_c41a5d9';
                break;
            default:
                $systemName = $newName;
                break;
        }

        return [$newName, $systemName];
    }

}
