<?php

namespace Npaf\Authentication;

use Npaf\Npaf;
use Npaf\NpafApp;
use Npaf\NpafRequest;
use Npaf\NpafResponse;
use Npaf\NpafClient;
use Npaf\Exceptions\NpafResponseException;
use Npaf\Exceptions\NpafSDKException;

/**
 * Class OAuth2Client
 *
 */
class OAuth2Client
{
    /**
     *
     * Todo: change this URL
     *
     */

    /**
     * @const string The base authorization URL.
     */
    const BASE_AUTHORIZATION_URL = 'http://npaf.vm';

    /**
     * @const string The authorization method.
     */
    const AUTHORIZATION_METHOD = 'code';


    /**
     * @const string The authorization relative path.
     */
    const AUTH_RELATIVE_PATH = 'dialog/oauth';


    protected $replacedAuthUrl = null;

    /**
     * The NpafApp entity.
     *
     * @var NpafApp
     */
    protected $app;

    /**
     * The Npaf client.
     *
     * @var NpafClient
     */
    protected $client;

    /**
     * The version of the Woar API to use.
     *
     * @var string
     */
    protected $woarVersioin;

    /**
     * The last request sent to Woar.
     *
     * @var NpafRequest|null
     */
    protected $lastRequest;

    /**
     * @param NpafApp    $app
     * @param NpafClient $client
     * @param string|null    $woarVersioin The version of the Woar API to use.
     */
    public function __construct(NpafApp $app, NpafClient $client, $woarVersioin = null)
    {
        $this->app = $app;
        $this->client = $client;
        $this->woarVersioin = $woarVersioin ?: Npaf::DEFAULT_GRAPH_VERSION;
    }

    /**
     * @return null
     */
    public function getReplacedAuthUrl()
    {
        return $this->replacedAuthUrl;
    }

    /**
     * @param null $replacedAuthUrl
     */
    public function setReplacedAuthUrl($replacedAuthUrl): void
    {
        $this->replacedAuthUrl = $replacedAuthUrl;
    }

    /**
     * Returns the last NpafRequest that was sent.
     * Useful for debugging and testing.
     *
     * @return NpafRequest|null
     */
    public function getLastRequest()
    {
        return $this->lastRequest;
    }

    /**
     * Get the metadata associated with the access token.
     *
     * @param AccessToken|string $accessToken The access token to debug.
     *
     * @return AccessTokenMetadata
     */
    public function debugToken($accessToken)
    {
        $accessToken = $accessToken instanceof AccessToken ? $accessToken->getValue() : $accessToken;
        $params = ['input_token' => $accessToken];

        $params += $this->getClientParams();

        $this->lastRequest = new NpafRequest(
            $this->app,
            $this->app->getAccessToken(),
            'POST',
            '/debug_token',
            $params,
            null,
            $this->woarVersioin
        );
        $response = $this->client->sendRequest($this->lastRequest);
        $metadata = $response->getDecodedBody();

        return new AccessTokenMetadata($metadata);
    }

    /**
     * Generates an authorization URL to begin the process of authenticating a user.
     *
     * @param string $redirectUrl The callback URL to redirect to.
     * @param string $state       The CSPRNG-generated CSRF value.
     * @param array  $scope       An array of permissions to request.
     * @param array  $params      An array of parameters to generate URL.
     * @param string $separator   The separator to use in http_build_query().
     *
     * @return string
     */
    public function getAuthorizationUrl($redirectUrl, $state, array $scope = [], array $params = [], $separator = '&')
    {
        $params += [
            'client_id' => $this->app->getId(),
            'state' => $state,
            'response_type' => static::AUTHORIZATION_METHOD,
            'sdk' => 'php-sdk-' . Npaf::VERSION,
            'redirect_uri' => $redirectUrl,
            'scope' => implode(',', $scope)
        ];

        $authBaseUrl = $this->getReplacedAuthUrl() ?: static::BASE_AUTHORIZATION_URL;

        return $authBaseUrl . '/' . $this->woarVersioin . '/' . static::AUTH_RELATIVE_PATH . '?' . http_build_query($params, null, $separator);
    }

    /**
     * Get a valid access token from a code.
     *
     * @param string $code
     * @param string $redirectUri
     *
     * @return AccessToken
     *
     * @throws NpafSDKException
     */
    public function getAccessTokenFromCode($code, $redirectUri = '')
    {
        $params = [
            'code' => $code,
            'redirect_uri' => $redirectUri,
        ];

        return $this->requestAnAccessToken($params);
    }

    /**
     * Exchanges a short-lived access token with a long-lived access token.
     *
     * @param AccessToken|string $accessToken
     *
     * @return AccessToken
     *
     * @throws NpafSDKException
     */
    public function getLongLivedAccessToken($accessToken)
    {
        $accessToken = $accessToken instanceof AccessToken ? $accessToken->getValue() : $accessToken;
        $params = [
            'grant_type' => 'fb_exchange_token',
            'fb_exchange_token' => $accessToken,
        ];

        return $this->requestAnAccessToken($params);
    }

    /**
     * Get a valid code from an access token.
     *
     * @param AccessToken|string $accessToken
     * @param string             $redirectUri
     *
     * @return AccessToken
     *
     * @throws NpafSDKException
     */
    public function getCodeFromLongLivedAccessToken($accessToken, $redirectUri = '')
    {
        $params = [
            'redirect_uri' => $redirectUri,
        ];

        $response = $this->sendRequestWithClientParams('/oauth/client_code', $params, $accessToken);
        $data = $response->getDecodedBody();

        if (!isset($data['code'])) {
            throw new NpafSDKException('Code was not returned from Woar.', 401);
        }

        return $data['code'];
    }

    /**
     * Send a request to the OAuth endpoint.
     *
     * @param array $params
     *
     * @return AccessToken
     *
     * @throws NpafSDKException
     */
    protected function requestAnAccessToken(array $params)
    {
        $response = $this->sendRequestWithClientParams('/oauth/access_token', $params);
        $data = $response->getDecodedBody();

        if (!isset($data['access_token'])) {
            throw new NpafSDKException('Access token was not returned from Woar.', 401);
        }

        // Woar returns two different key names for expiration time
        // on the same endpoint. Doh! :/
        $expiresAt = 0;
        if (isset($data['expires'])) {
            // For exchanging a short lived token with a long lived token.
            // The expiration time in seconds will be returned as "expires".
            $expiresAt = time() + $data['expires'];
        } elseif (isset($data['expires_in'])) {
            // For exchanging a code for a short lived access token.
            // The expiration time in seconds will be returned as "expires_in".
            // See: https://developers.Npaf.com/docs/Npaf-login/access-tokens#long-via-code
            $expiresAt = time() + $data['expires_in'];
        }

        return new AccessToken($data['access_token'], $expiresAt);
    }

    /**
     * Send a request to Woar with an app access token.
     *
     * @param string                  $endpoint
     * @param array                   $params
     * @param AccessToken|string|null $accessToken
     *
     * @return NpafResponse
     *
     * @throws NpafResponseException
     */
    protected function sendRequestWithClientParams($endpoint, array $params, $accessToken = null)
    {
        $params += $this->getClientParams();

        $accessToken = $accessToken ?: $this->app->getAccessToken();

        $this->lastRequest = new NpafRequest(
            $this->app,
            $accessToken,
            'POST',
            $endpoint,
            $params,
            null,
            $this->woarVersioin
        );

        return $this->client->sendRequest($this->lastRequest);
    }

    /**
     * Returns the client_* params for OAuth requests.
     *
     * @return array
     */
    protected function getClientParams()
    {
        return [
            'client_id' => $this->app->getId(),
            'client_secret' => $this->app->getSecret(),
        ];
    }
}
