<?php

namespace Npaf\Http;

/**
 * Interface
 *
 */
interface RequestBodyInterface
{
    /**
     * Get the body of the request to send to Woar.
     *
     * @return string
     */
    public function getBody();
}
