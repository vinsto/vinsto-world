<?php

namespace Npaf;

use Npaf\Authentication\OAuth2Client;
use Npaf\Exceptions\NpafSDKException;
use Npaf\Helpers\NpafRedirectLoginHelper;
use Npaf\HttpClients\HttpClientsFactory;
use Npaf\PseudoRandomString\PseudoRandomStringGeneratorFactory;
use Npaf\PersistentData\PersistentDataFactory;
use Npaf\Url\NpafUrlDetectionHandler;
use Npaf\Url\UrlDetectionInterface;

class Npaf
{
    /**
     * @const string Version number of the Npaf PHP SDK.
     */
    const VERSION = '0.0.1';

    /**
     * @const string The name of the environment variable that contains the app ID.
     */
    const APP_ID_ENV_NAME = 'NPAF_APP_ID';

    /**
     * @const string The name of the environment variable that contains the app secret.
     */
    const APP_SECRET_ENV_NAME = 'NPAF_APP_SECRET';

    /**
     * @const string Default Woar for requests.
     */
    const DEFAULT_WOAR_VERSION = '0.1';

    /**
     * @var NpafApp The NpafApp entity.
     */
    protected $app;

    /**
     * @var NpafClient The Npaf client service.
     */
    protected $client;

    /**
     * @var OAuth2Client The OAuth 2.0 client service.
     */
    protected $oAuth2Client;

    /**
     * @var UrlDetectionInterface|null The URL detection handler.
     */
    protected $urlDetectionHandler;

    /**
     * @var PseudoRandomStringGeneratorInterface|null The cryptographically secure pseudo-random string generator.
     */
    protected $pseudoRandomStringGenerator;

    /**
     * @var AccessToken|null The default access token to use with requests.
     */
    protected $defaultAccessToken;

    /**
     * @var string|null The default Npaf we want to use.
     */
    protected $woarVersion;

    /**
     * @var PersistentDataInterface|null The persistent data handler.
     */
    protected $persistentDataHandler;

    /**
     * @var NpafResponse|NpafBatchResponse|null Stores the last request made to Woar.
     */
    protected $lastResponse;

    /**
     * Instantiates a new Npaf super-class object.
     *
     * @param array $config
     *
     * @throws NpafSDKException
     */
    public function __construct(array $config = [])
    {
        $config = array_merge([
            'app_id' => getenv(static::APP_ID_ENV_NAME),
            'app_secret' => getenv(static::APP_SECRET_ENV_NAME),
            'default_woar_version' => static::DEFAULT_WOAR_VERSION,
            'enable_beta_mode' => false,
            'http_client_handler' => null,
            'persistent_data_handler' => null,
            'pseudo_random_string_generator' => null,
            'url_detection_handler' => null,
        ], $config);

        if (!$config['app_id']) {
            throw new NpafSDKException('Required "app_id" key not supplied in config and could not find fallback environment variable "' . static::APP_ID_ENV_NAME . '"');
        }
        if (!$config['app_secret']) {
            throw new NpafSDKException('Required "app_secret" key not supplied in config and could not find fallback environment variable "' . static::APP_SECRET_ENV_NAME . '"');
        }

        $this->app = new NpafApp($config['app_id'], $config['app_secret']);
        $this->client = new NpafClient(
            HttpClientsFactory::createHttpClient($config['http_client_handler'])
        );

        if (!empty($config['woar_url'])) {
            $this->client->setReplacedWoarUrl($config['woar_url']);
        }

        $this->woarVersion = $config['default_woar_version'];

        $this->oAuth2Client = new OAuth2Client($this->app, $this->client, $this->woarVersion);
        if (!empty($config['oauth2_url'])) {
            $this->oAuth2Client->setReplacedAuthUrl($config['oauth2_url']);
        }

        $this->pseudoRandomStringGenerator = PseudoRandomStringGeneratorFactory::createPseudoRandomStringGenerator(
            $config['pseudo_random_string_generator']
        );

        $this->persistentDataHandler = PersistentDataFactory::createPersistentDataHandler(
            $config['persistent_data_handler']
        );

        $this->setUrlDetectionHandler($config['url_detection_handler'] ?: new NpafUrlDetectionHandler());

        /*        if (isset($config['default_access_token'])) {
                    $this->setDefaultAccessToken($config['default_access_token']);
                }*/
    }

    /**
     * Returns the NpafApp entity.
     *
     * @return NpafApp
     */
    public function getApp()
    {
        return $this->app;
    }

    /**
     * Returns the NpafClient service.
     *
     * @return NpafClient
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Returns the OAuth 2.0 client service.
     *
     * @return OAuth2Client
     */
    public function getOAuth2Client()
    {
        return $this->oAuth2Client;
    }

    /**
     * Returns the last response returned from Woar.
     *
     * @return NpafResponse|NpafBatchResponse|null
     */
    public function getLastResponse()
    {
        return $this->lastResponse;
    }

    /**
     * Returns the URL detection handler.
     *
     * @return UrlDetectionInterface
     */
    public function getUrlDetectionHandler()
    {
        return $this->urlDetectionHandler;
    }

    /**
     * Changes the URL detection handler.
     *
     * @param UrlDetectionInterface $urlDetectionHandler
     */
    private function setUrlDetectionHandler(UrlDetectionInterface $urlDetectionHandler)
    {
        $this->urlDetectionHandler = $urlDetectionHandler;
    }

    /**
     * Returns the default AccessToken entity.
     *
     * @return AccessToken|null
     */
    public function getDefaultAccessToken()
    {
        return $this->defaultAccessToken;
    }

    /**
     * Sets the default access token to use with requests.
     *
     * @param AccessToken|string $accessToken The access token to save.
     *
     * @throws \InvalidArgumentException
     */
    public function setDefaultAccessToken($accessToken)
    {
        if (is_string($accessToken)) {
            $this->defaultAccessToken = new AccessToken($accessToken);

            return;
        }

        if ($accessToken instanceof AccessToken) {
            $this->defaultAccessToken = $accessToken;

            return;
        }

        throw new \InvalidArgumentException('The default access token must be of type "string" or Npaf\AccessToken');
    }

    /**
     * Returns the default Npaf.
     *
     * @return string
     */
    public function getDefaultWoarVersion()
    {
        return $this->woarVersion;
    }

    /**
     * Returns the redirect login helper.
     *
     * @return NpafRedirectLoginHelper
     */
    public function getRedirectLoginHelper()
    {
        return new NpafRedirectLoginHelper(
            $this->getOAuth2Client(),
            $this->persistentDataHandler,
            $this->urlDetectionHandler,
            $this->pseudoRandomStringGenerator
        );
    }


    /**
     * Sends a GET request to Woar and returns the result.
     *
     * @param string                  $endpoint
     * @param AccessToken|string|null $accessToken
     * @param string|null             $eTag
     * @param string|null             $woarVersion
     *
     * @return NpafResponse
     *
     * @throws NpafSDKException
     */
    public function get($endpoint, $accessToken = null, $eTag = null, $woarVersion = null)
    {
        return $this->sendRequest(
            'GET',
            $endpoint,
            $params = [],
            $accessToken,
            $eTag,
            $woarVersion
        );
    }

    /**
     * Sends a POST request to Woar and returns the result.
     *
     * @param string                  $endpoint
     * @param array                   $params
     * @param AccessToken|string|null $accessToken
     * @param string|null             $eTag
     * @param string|null             $woarVersion
     *
     * @return NpafResponse
     *
     * @throws NpafSDKException
     */
    public function post($endpoint, array $params = [], $accessToken = null, $eTag = null, $woarVersion = null)
    {
        return $this->sendRequest(
            'POST',
            $endpoint,
            $params,
            $accessToken,
            $eTag,
            $woarVersion
        );
    }

    /**
     * Sends a DELETE request to Woar and returns the result.
     *
     * @param string                  $endpoint
     * @param array                   $params
     * @param AccessToken|string|null $accessToken
     * @param string|null             $eTag
     * @param string|null             $woarVersion
     *
     * @return NpafResponse
     *
     * @throws NpafSDKException
     */
    public function delete($endpoint, array $params = [], $accessToken = null, $eTag = null, $woarVersion = null)
    {
        return $this->sendRequest(
            'DELETE',
            $endpoint,
            $params,
            $accessToken,
            $eTag,
            $woarVersion
        );
    }


    /**
     * Sends a request to Woar and returns the result.
     *
     * @param string                  $method
     * @param string                  $endpoint
     * @param array                   $params
     * @param AccessToken|string|null $accessToken
     * @param string|null             $eTag
     * @param string|null             $woarVersion
     *
     * @return NpafResponse
     *
     * @throws NpafSDKException
     */
    public function sendRequest($method, $endpoint, array $params = [], $accessToken = null, $eTag = null, $woarVersion = null)
    {
        $accessToken = $accessToken ?: $this->defaultAccessToken;
        $woarVersion = $woarVersion ?: $this->woarVersion;
        $request = $this->request($method, $endpoint, $params, $accessToken, $eTag, $woarVersion);

        return $this->lastResponse = $this->client->sendRequest($request);
    }



    /**
     * Instantiates a new NpafRequest entity.
     *
     * @param string                  $method
     * @param string                  $endpoint
     * @param array                   $params
     * @param AccessToken|string|null $accessToken
     * @param string|null             $eTag
     * @param string|null             $woarVersion
     *
     * @return NpafRequest
     *
     * @throws NpafSDKException
     */
    public function request($method, $endpoint, array $params = [], $accessToken = null, $eTag = null, $woarVersion = null)
    {
        $accessToken = $accessToken ?: $this->defaultAccessToken;
        $woarVersion = $woarVersion ?: $this->woarVersion;

        return new NpafRequest(
            $this->app,
            $accessToken,
            $method,
            $endpoint,
            $params,
            $eTag,
            $woarVersion
        );
    }

}
