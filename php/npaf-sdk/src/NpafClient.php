<?php
/**
 * Created by PhpStorm.
 * User: nwe
 * Date: 13.11.18
 * Time: 09:56
 */

namespace Npaf;

use Npaf\HttpClients\NpafHttpClientInterface;
use Npaf\HttpClients\NpafCurlHttpClient;
use Npaf\HttpClients\NpafStreamHttpClient;
use Npaf\Exceptions\NpafSDKException;

class NpafClient
{
    /**
     * @const string Production Woar URL.
     */
    const BASE_WOAR_URL = 'http://npaf.vm/woar';

    /**
     * @const int The timeout in seconds for a normal request.
     */
    const DEFAULT_REQUEST_TIMEOUT = 60;

    /**
     * @var NpafHttpClientInterface HTTP client handler.
     */
    protected $httpClientHandler;

    protected $replacedWoarUrl = null;

    /**
     * @var int The number of calls that have been made to Woar.
     */
    public static $requestCount = 0;

    /**
     * Instantiates a new NpafClient object.
     *
     * @param NpafHttpClientInterface|null $httpClientHandler
     *
     */
    public function __construct(NpafHttpClientInterface $httpClientHandler = null)
    {
        $this->httpClientHandler = $httpClientHandler ?: $this->detectHttpClientHandler();
    }

    /**
     * Sets the HTTP client handler.
     *
     * @param NpafHttpClientInterface $httpClientHandler
     */
    public function setHttpClientHandler(NpafHttpClientInterface $httpClientHandler)
    {
        $this->httpClientHandler = $httpClientHandler;
    }

    /**
     * Returns the HTTP client handler.
     *
     * @return NpafHttpClientInterface
     */
    public function getHttpClientHandler()
    {
        return $this->httpClientHandler;
    }

    /**
     * Detects which HTTP client handler to use.
     *
     * @return NpafHttpClientInterface
     */
    public function detectHttpClientHandler()
    {
        return extension_loaded('curl') ? new NpafCurlHttpClient() : new NpafStreamHttpClient();
    }

    /**
     * @return null
     */
    public function getReplacedWoarUrl()
    {
        return $this->replacedWoarUrl;
    }

    /**
     * @param null $replacedWoarUrl
     */
    public function setReplacedWoarUrl($replacedWoarUrl): void
    {
        $this->replacedWoarUrl = $replacedWoarUrl;
    }

    /**
     * Returns the base Woar URL.
     *
     * @return string
     */
    public function getBaseWoarUrl()
    {
        return static::BASE_WOAR_URL;
    }

    /**
     * Prepares the request for sending to the client handler.
     *
     * @param NpafRequest $request
     *
     * @return array
     */
    public function prepareRequestMessage(NpafRequest $request)
    {
        if (!empty($replacedWoarUrl = $this->getReplacedWoarUrl())) {
            $url = $replacedWoarUrl . $request->getUrl();
        }else {
            $url = $this->getBaseWoarUrl() . $request->getUrl();
        }

        // If we're sending files they should be sent as multipart/form-data
        if ($request->containsFileUploads()) {
            $requestBody = $request->getMultipartBody();
            $request->setHeaders([
                'Content-Type' => 'multipart/form-data; boundary=' . $requestBody->getBoundary(),
            ]);
        } else {
            $requestBody = $request->getUrlEncodedBody();
            $request->setHeaders([
                'Content-Type' => 'application/x-www-form-urlencoded',
            ]);
        }

        return [
            $url,
            $request->getMethod(),
            $request->getHeaders(),
            $requestBody->getBody(),
        ];
    }

    /**
     * Makes the request to Woar and returns the result.
     *
     * @param NpafRequest $request
     *
     * @return NpafResponse
     *
     * @throws NpafSDKException
     */
    public function sendRequest(NpafRequest $request)
    {
        if (get_class($request) === 'Npaf\NpafRequest') {
            $request->validateAccessToken();
        }

        list($url, $method, $headers, $body) = $this->prepareRequestMessage($request);

        // Since file uploads can take a while, we need to give more time for uploads
        $timeOut = static::DEFAULT_REQUEST_TIMEOUT;
        if ($request->containsFileUploads()) {
            $timeOut = static::DEFAULT_FILE_UPLOAD_REQUEST_TIMEOUT;
        } elseif ($request->containsVideoUploads()) {
            $timeOut = static::DEFAULT_VIDEO_UPLOAD_REQUEST_TIMEOUT;
        }

        // Should throw `NpafSDKException` exception on HTTP client error.
        // Don't catch to allow it to bubble up.
        $rawResponse = $this->httpClientHandler->send($url, $method, $body, $headers, $timeOut);

        static::$requestCount++;

        $returnResponse = new NpafResponse(
            $request,
            $rawResponse->getBody(),
            $rawResponse->getHttpResponseCode(),
            $rawResponse->getHeaders()
        );

        if ($returnResponse->isError()) {
            throw $returnResponse->getThrownException();
        }

        return $returnResponse;
    }


}
