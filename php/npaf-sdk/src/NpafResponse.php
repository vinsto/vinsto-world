<?php

namespace Npaf;

use Npaf\WoarNodes\WoarNodeFactory;
use Npaf\Exceptions\NpafResponseException;
use Npaf\Exceptions\NpafSDKException;

/**
 * Class NpafResponse
 *
 */
class NpafResponse
{
    /**
     * @var int The HTTP status code response from Woar.
     */
    protected $httpStatusCode;

    /**
     * @var array The headers returned from Woar.
     */
    protected $headers;

    /**
     * @var string The raw body of the response from Woar.
     */
    protected $body;

    /**
     * @var array The decoded body of the Woar response.
     */
    protected $decodedBody = [];

    /**
     * @var NpafRequest The original request that returned this response.
     */
    protected $request;

    /**
     * @var NpafSDKException The exception thrown by this request.
     */
    protected $thrownException;

    /**
     * Creates a new Response entity.
     *
     * @param NpafRequest $request
     * @param string|null     $body
     * @param int|null        $httpStatusCode
     * @param array|null      $headers
     */
    public function __construct(NpafRequest $request, $body = null, $httpStatusCode = null, array $headers = [])
    {
        $this->request = $request;
        $this->body = $body;
        $this->httpStatusCode = $httpStatusCode;
        $this->headers = $headers;

        $this->decodeBody();
    }

    /**
     * Return the original request that returned this response.
     *
     * @return NpafRequest
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * Return the NpafApp entity used for this response.
     *
     * @return NpafApp
     */
    public function getApp()
    {
        return $this->request->getApp();
    }

    /**
     * Return the access token that was used for this response.
     *
     * @return string|null
     */
    public function getAccessToken()
    {
        return $this->request->getAccessToken();
    }

    /**
     * Return the HTTP status code for this response.
     *
     * @return int
     */
    public function getHttpStatusCode()
    {
        return $this->httpStatusCode;
    }

    /**
     * Return the HTTP headers for this response.
     *
     * @return array
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * Return the raw body response.
     *
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Return the decoded body response.
     *
     * @return array
     */
    public function getDecodedBody()
    {
        return $this->decodedBody;
    }

    /**
     * Get the app secret proof that was used for this response.
     *
     * @return string|null
     */
    public function getAppSecretProof()
    {
        return $this->request->getAppSecretProof();
    }

    /**
     * Get the ETag associated with the response.
     *
     * @return string|null
     */
    public function getETag()
    {
        return isset($this->headers['ETag']) ? $this->headers['ETag'] : null;
    }

    /**
     * Get the version of Woar that returned this response.
     *
     * @return string|null
     */
    public function getWoarVersioin()
    {
        return isset($this->headers['Npaf-API-Version']) ? $this->headers['Npaf-API-Version'] : null;
    }

    /**
     * Returns true if Woar returned an error message.
     *
     * @return boolean
     */
    public function isError()
    {
        return isset($this->decodedBody['error']);
    }

    /**
     * Throws the exception.
     *
     * @throws NpafSDKException
     */
    public function throwException()
    {
        throw $this->thrownException;
    }

    /**
     * Instantiates an exception to be thrown later.
     */
    public function makeException()
    {
        $this->thrownException = NpafResponseException::create($this);
    }

    /**
     * Returns the exception that was thrown for this request.
     *
     * @return NpafResponseException|null
     */
    public function getThrownException()
    {
        return $this->thrownException;
    }

    /**
     * Convert the raw response into an array if possible.
     *
     * Woar will return 2 types of responses:
     * - JSON(P)
     *    Most responses from Woar are JSON(P)
     * - application/x-www-form-urlencoded key/value pairs
     *    Happens on the `/oauth/access_token` endpoint when exchanging
     *    a short-lived access token for a long-lived access token
     * - And sometimes nothing :/ but that'd be a bug.
     */
    public function decodeBody()
    {
        $this->decodedBody = json_decode($this->body, true);

        if ($this->decodedBody === null) {
            $this->decodedBody = [];
            parse_str($this->body, $this->decodedBody);
        } elseif (is_bool($this->decodedBody)) {
            // Backwards compatibility for Woar < 2.1.
            // Mimics 2.1 responses.
            // @TODO Remove this after Woar 2.0 is no longer supported
            $this->decodedBody = ['success' => $this->decodedBody];
        } elseif (is_numeric($this->decodedBody)) {
            $this->decodedBody = ['id' => $this->decodedBody];
        }

        if (!is_array($this->decodedBody)) {
            $this->decodedBody = [];
        }

        if ($this->isError()) {
            $this->makeException();
        }
    }

    /**
     * Instantiate a new WoarObject from response.
     *
     * @param string|null $subclassName The WoarNode subclass to cast to.
     *
     * @return \Npaf\WoarNodes\WoarObject
     *
     * @throws NpafSDKException
     *
     * @deprecated 5.0.0 getWoarObject() has been renamed to getWoarNode()
     * @todo v6: Remove this method
     */
    public function getWoarObject($subclassName = null)
    {
        return $this->getWoarNode($subclassName);
    }

    /**
     * Instantiate a new WoarNode from response.
     *
     * @param string|null $subclassName The WoarNode subclass to cast to.
     *
     * @return \Npaf\WoarNodes\WoarNode
     *
     * @throws NpafSDKException
     */
    public function getWoarNode($subclassName = null)
    {
        $factory = new WoarNodeFactory($this);

        return $factory->makeWoarNode($subclassName);
    }

    /**
     * Convenience method for creating a WoarAlbum collection.
     *
     * @return \Npaf\WoarNodes\WoarAlbum
     *
     * @throws NpafSDKException
     */
    public function getWoarAlbum()
    {
        $factory = new WoarNodeFactory($this);

        return $factory->makeWoarAlbum();
    }

    /**
     * Convenience method for creating a WoarPage collection.
     *
     * @return \Npaf\WoarNodes\WoarPage
     *
     * @throws NpafSDKException
     */
    public function getWoarPage()
    {
        $factory = new WoarNodeFactory($this);

        return $factory->makeWoarPage();
    }

    /**
     * Convenience method for creating a WoarSessionInfo collection.
     *
     * @return \Npaf\WoarNodes\WoarSessionInfo
     *
     * @throws NpafSDKException
     */
    public function getWoarSessionInfo()
    {
        $factory = new WoarNodeFactory($this);

        return $factory->makeWoarSessionInfo();
    }

    /**
     * Convenience method for creating a WoarUser collection.
     *
     * @return \Npaf\WoarNodes\WoarUser
     *
     * @throws NpafSDKException
     */
    public function getWoarUser()
    {
        $factory = new WoarNodeFactory($this);

        return $factory->makeWoarUser();
    }

    /**
     * Convenience method for creating a WoarEvent collection.
     *
     * @return \Npaf\WoarNodes\WoarEvent
     *
     * @throws NpafSDKException
     */
    public function getWoarEvent()
    {
        $factory = new WoarNodeFactory($this);

        return $factory->makeWoarEvent();
    }

    /**
     * Convenience method for creating a WoarGroup collection.
     *
     * @return \Npaf\WoarNodes\WoarGroup
     *
     * @throws NpafSDKException
     */
    public function getWoarGroup()
    {
        $factory = new WoarNodeFactory($this);

        return $factory->makeWoarGroup();
    }

    /**
     * Instantiate a new WoarList from response.
     *
     * @param string|null $subclassName The WoarNode subclass to cast list items to.
     * @param boolean     $auto_prefix  Toggle to auto-prefix the subclass name.
     *
     * @return \Npaf\WoarNodes\WoarList
     *
     * @throws NpafSDKException
     *
     * @deprecated 5.0.0 getWoarList() has been renamed to getWoarEdge()
     * @todo v6: Remove this method
     */
    public function getWoarList($subclassName = null, $auto_prefix = true)
    {
        return $this->getWoarEdge($subclassName, $auto_prefix);
    }

    /**
     * Instantiate a new WoarEdge from response.
     *
     * @param string|null $subclassName The WoarNode subclass to cast list items to.
     * @param boolean     $auto_prefix  Toggle to auto-prefix the subclass name.
     *
     * @return \Npaf\WoarNodes\WoarEdge
     *
     * @throws NpafSDKException
     */
    public function getWoarEdge($subclassName = null, $auto_prefix = true)
    {
        $factory = new WoarNodeFactory($this);

        return $factory->makeWoarEdge($subclassName, $auto_prefix);
    }
}
