<?php

namespace Npaf\PersistentData;

use Npaf\Exceptions\NpafSDKException;

class NpafSessionPersistentDataHandler implements PersistentDataInterface
{
    const SESSION_PREFIX = 'UCQTH_';

    /**
     * Init the session handler.
     *
     * @param boolean $enableSessionCheck
     *
     * @throws NpafSDKException
     */
    public function __construct($enableSessionCheck = true)
    {
        if ($enableSessionCheck && session_status() !== PHP_SESSION_ACTIVE) {
            throw new NpafSDKException(
                'Sessions are not active. Please make sure session_start() is at the top of your script.',
                720
            );
        }
    }

    /**
     * @inheritdoc
     */
    public function get($key)
    {
        if (isset($_SESSION[self::SESSION_PREFIX . $key])) {
            return $_SESSION[self::SESSION_PREFIX . $key];
        }

        return null;
    }

    /**
     * @inheritdoc
     */
    public function set($key, $value)
    {
        $_SESSION[self::SESSION_PREFIX . $key] = $value;
    }
}
