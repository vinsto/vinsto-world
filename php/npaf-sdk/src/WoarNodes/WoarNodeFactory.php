<?php

namespace Npaf\WoarNodes;

use Npaf\NpafResponse;
use Npaf\Exceptions\NpafSDKException;

/**
 * Class WoarNodeFactory
 *
 *
 * ## Assumptions ##
 * WoarEdge - is ALWAYS a numeric array
 * WoarEdge - is ALWAYS an array of WoarNode types
 * WoarNode - is ALWAYS an associative array
 * WoarNode - MAY contain WoarNode's "recurrable"
 * WoarNode - MAY contain WoarEdge's "recurrable"
 * WoarNode - MAY contain DateTime's "primitives"
 * WoarNode - MAY contain string's "primitives"
 */
class WoarNodeFactory
{
    /**
     * @const string The base woar object class.
     */
    const BASE_WOAR_NODE_CLASS = '\Npaf\WoarNodes\WoarNode';

    /**
     * @const string The base woar edge class.
     */
    const BASE_WOAR_EDGE_CLASS = '\Npaf\WoarNodes\WoarEdge';

    /**
     * @const string The woar object prefix.
     */
    const BASE_WOAR_OBJECT_PREFIX = '\Npaf\WoarNodes\\';

    /**
     * @var NpafResponse The response entity from Woar.
     */
    protected $response;

    /**
     * @var array The decoded body of the NpafResponse entity from Woar.
     */
    protected $decodedBody;

    /**
     * Init this Woar object.
     *
     * @param NpafResponse $response The response entity from Woar.
     */
    public function __construct(NpafResponse $response)
    {
        $this->response = $response;
        $this->decodedBody = $response->getDecodedBody();
    }

    /**
     * Tries to convert a NpafResponse entity into a WoarNode.
     *
     * @param string|null $subclassName The WoarNode sub class to cast to.
     *
     * @return WoarNode
     *
     * @throws NpafSDKException
     */
    public function makeWoarNode($subclassName = null)
    {
        $this->validateResponseAsArray();
        $this->validateResponseCastableAsWoarNode();

        return $this->castAsWoarNodeOrWoarEdge($this->decodedBody, $subclassName);
    }

    /**
     * Convenience method for creating a WoarAchievement collection.
     *
     * @return WoarAchievement
     *
     * @throws NpafSDKException
     */
    public function makeWoarAchievement()
    {
        return $this->makeWoarNode(static::BASE_WOAR_OBJECT_PREFIX . 'WoarAchievement');
    }

    /**
     * Convenience method for creating a WoarAlbum collection.
     *
     * @return WoarAlbum
     *
     * @throws NpafSDKException
     */
    public function makeWoarAlbum()
    {
        return $this->makeWoarNode(static::BASE_WOAR_OBJECT_PREFIX . 'WoarAlbum');
    }

    /**
     * Convenience method for creating a WoarPage collection.
     *
     * @return WoarPage
     *
     * @throws NpafSDKException
     */
    public function makeWoarPage()
    {
        return $this->makeWoarNode(static::BASE_WOAR_OBJECT_PREFIX . 'WoarPage');
    }

    /**
     * Convenience method for creating a WoarSessionInfo collection.
     *
     * @return WoarSessionInfo
     *
     * @throws NpafSDKException
     */
    public function makeWoarSessionInfo()
    {
        return $this->makeWoarNode(static::BASE_WOAR_OBJECT_PREFIX . 'WoarSessionInfo');
    }

    /**
     * Convenience method for creating a WoarUser collection.
     *
     * @return WoarUser
     *
     * @throws NpafSDKException
     */
    public function makeWoarUser()
    {
        return $this->makeWoarNode(static::BASE_WOAR_OBJECT_PREFIX . 'WoarUser');
    }

    /**
     * Convenience method for creating a WoarEvent collection.
     *
     * @return WoarEvent
     *
     * @throws NpafSDKException
     */
    public function makeWoarEvent()
    {
        return $this->makeWoarNode(static::BASE_WOAR_OBJECT_PREFIX . 'WoarEvent');
    }

    /**
     * Convenience method for creating a WoarGroup collection.
     *
     * @return WoarGroup
     *
     * @throws NpafSDKException
     */
    public function makeWoarGroup()
    {
        return $this->makeWoarNode(static::BASE_WOAR_OBJECT_PREFIX . 'WoarGroup');
    }

    /**
     * Tries to convert a NpafResponse entity into a WoarEdge.
     *
     * @param string|null $subclassName The WoarNode sub class to cast the list items to.
     * @param boolean     $auto_prefix  Toggle to auto-prefix the subclass name.
     *
     * @return WoarEdge
     *
     * @throws NpafSDKException
     */
    public function makeWoarEdge($subclassName = null, $auto_prefix = true)
    {
        $this->validateResponseAsArray();
        $this->validateResponseCastableAsWoarEdge();

        if ($subclassName && $auto_prefix) {
            $subclassName = static::BASE_WOAR_OBJECT_PREFIX . $subclassName;
        }

        return $this->castAsWoarNodeOrWoarEdge($this->decodedBody, $subclassName);
    }

    /**
     * Validates the decoded body.
     *
     * @throws NpafSDKException
     */
    public function validateResponseAsArray()
    {
        if (!is_array($this->decodedBody)) {
            throw new NpafSDKException('Unable to get response from Woar as array.', 620);
        }
    }

    /**
     * Validates that the return data can be cast as a WoarNode.
     *
     * @throws NpafSDKException
     */
    public function validateResponseCastableAsWoarNode()
    {
        if (isset($this->decodedBody['data']) && static::isCastableAsWoarEdge($this->decodedBody['data'])) {
            throw new NpafSDKException(
                'Unable to convert response from Woar to a WoarNode because the response looks like a WoarEdge. Try using WoarNodeFactory::makeWoarEdge() instead.',
                620
            );
        }
    }

    /**
     * Validates that the return data can be cast as a WoarEdge.
     *
     * @throws NpafSDKException
     */
    public function validateResponseCastableAsWoarEdge()
    {
        if (!(isset($this->decodedBody['data']) && static::isCastableAsWoarEdge($this->decodedBody['data']))) {
            throw new NpafSDKException(
                'Unable to convert response from Woar to a WoarEdge because the response does not look like a WoarEdge. Try using WoarNodeFactory::makeWoarNode() instead.',
                620
            );
        }
    }

    /**
     * Safely instantiates a WoarNode of $subclassName.
     *
     * @param array       $data         The array of data to iterate over.
     * @param string|null $subclassName The subclass to cast this collection to.
     *
     * @return WoarNode
     *
     * @throws NpafSDKException
     */
    public function safelyMakeWoarNode(array $data, $subclassName = null)
    {
        $subclassName = $subclassName ?: static::BASE_WOAR_NODE_CLASS;
        static::validateSubclass($subclassName);

        // Remember the parent node ID
        $parentNodeId = isset($data['id']) ? $data['id'] : null;

        $items = [];

        foreach ($data as $k => $v) {
            // Array means could be recurable
            if (is_array($v)) {
                // Detect any smart-casting from the $woarObjectMap array.
                // This is always empty on the WoarNode collection, but subclasses can define
                // their own array of smart-casting types.
                $woarObjectMap = $subclassName::getObjectMap();
                $objectSubClass = isset($woarObjectMap[$k])
                    ? $woarObjectMap[$k]
                    : null;

                // Could be a WoarEdge or WoarNode
                $items[$k] = $this->castAsWoarNodeOrWoarEdge($v, $objectSubClass, $k, $parentNodeId);
            } else {
                $items[$k] = $v;
            }
        }

        return new $subclassName($items);
    }

    /**
     * Takes an array of values and determines how to cast each node.
     *
     * @param array       $data         The array of data to iterate over.
     * @param string|null $subclassName The subclass to cast this collection to.
     * @param string|null $parentKey    The key of this data (Woar edge).
     * @param string|null $parentNodeId The parent Woar node ID.
     *
     * @return WoarNode|WoarEdge
     *
     * @throws NpafSDKException
     */
    public function castAsWoarNodeOrWoarEdge(array $data, $subclassName = null, $parentKey = null, $parentNodeId = null)
    {
        if (isset($data['data'])) {
            // Create WoarEdge
            if (static::isCastableAsWoarEdge($data['data'])) {
                return $this->safelyMakeWoarEdge($data, $subclassName, $parentKey, $parentNodeId);
            }
            // Sometimes Woar is a weirdo and returns a WoarNode under the "data" key
            $data = $data['data'];
        }

        // Create WoarNode
        return $this->safelyMakeWoarNode($data, $subclassName);
    }

    /**
     * Return an array of WoarNode's.
     *
     * @param array       $data         The array of data to iterate over.
     * @param string|null $subclassName The WoarNode subclass to cast each item in the list to.
     * @param string|null $parentKey    The key of this data (Woar edge).
     * @param string|null $parentNodeId The parent Woar node ID.
     *
     * @return WoarEdge
     *
     * @throws NpafSDKException
     */
    public function safelyMakeWoarEdge(array $data, $subclassName = null, $parentKey = null, $parentNodeId = null)
    {
        if (!isset($data['data'])) {
            throw new NpafSDKException('Cannot cast data to WoarEdge. Expected a "data" key.', 620);
        }

        $dataList = [];
        foreach ($data['data'] as $woarNode) {
            $dataList[] = $this->safelyMakeWoarNode($woarNode, $subclassName);
        }

        $metaData = $this->getMetaData($data);

        // We'll need to make an edge endpoint for this in case it's a WoarEdge (for cursor pagination)
        $parentWoarEdgeEndpoint = $parentNodeId && $parentKey ? '/' . $parentNodeId . '/' . $parentKey : null;
        $className = static::BASE_WOAR_EDGE_CLASS;

        return new $className($this->response->getRequest(), $dataList, $metaData, $parentWoarEdgeEndpoint, $subclassName);
    }

    /**
     * Get the meta data from a list in a Woar response.
     *
     * @param array $data The Woar response.
     *
     * @return array
     */
    public function getMetaData(array $data)
    {
        unset($data['data']);

        return $data;
    }

    /**
     * Determines whether or not the data should be cast as a WoarEdge.
     *
     * @param array $data
     *
     * @return boolean
     */
    public static function isCastableAsWoarEdge(array $data)
    {
        if ($data === []) {
            return true;
        }

        // Checks for a sequential numeric array which would be a WoarEdge
        return array_keys($data) === range(0, count($data) - 1);
    }

    /**
     * Ensures that the subclass in question is valid.
     *
     * @param string $subclassName The WoarNode subclass to validate.
     *
     * @throws NpafSDKException
     */
    public static function validateSubclass($subclassName)
    {
        if ($subclassName == static::BASE_WOAR_NODE_CLASS || is_subclass_of($subclassName, static::BASE_WOAR_NODE_CLASS)) {
            return;
        }

        throw new NpafSDKException('The given subclass "' . $subclassName . '" is not valid. Cannot cast to an object that is not a WoarNode subclass.', 620);
    }
}
