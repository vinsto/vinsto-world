<?php

namespace Npaf\WoarNodes;

/**
 * Class WoarUser
 *
 */
class WoarUser extends WoarNode
{
    /**
     * @var array Maps object key names to Woar object types.
     */
    protected static $woarObjectMap = [
        'hometown' => '\Npaf\WoarNodes\WoarPage',
        'location' => '\Npaf\WoarNodes\WoarPage',
        'significant_other' => '\Npaf\WoarNodes\WoarUser',
        'picture' => '\Npaf\WoarNodes\WoarPicture',
    ];

    /**
     * Returns the ID for the user as a string if present.
     *
     * @return string|null
     */
    public function getId()
    {
        return $this->getField('id');
    }

    /**
     * Returns the name for the user as a string if present.
     *
     * @return string|null
     */
    public function getName()
    {
        return $this->getField('name');
    }

    /**
     * Returns the first name for the user as a string if present.
     *
     * @return string|null
     */
    public function getFirstName()
    {
        return $this->getField('first_name');
    }

    /**
     * Returns the middle name for the user as a string if present.
     *
     * @return string|null
     */
    public function getMiddleName()
    {
        return $this->getField('middle_name');
    }

    /**
     * Returns the last name for the user as a string if present.
     *
     * @return string|null
     */
    public function getLastName()
    {
        return $this->getField('last_name');
    }

    /**
     * Returns the email for the user as a string if present.
     *
     * @return string|null
     */
    public function getEmail()
    {
        return $this->getField('email');
    }

    /**
     * Returns the gender for the user as a string if present.
     *
     * @return string|null
     */
    public function getGender()
    {
        return $this->getField('gender');
    }

    /**
     * Returns the Npaf URL for the user as a string if available.
     *
     * @return string|null
     */
    public function getLink()
    {
        return $this->getField('link');
    }

    /**
     * Returns the users birthday, if available.
     *
     * @return Birthday|null
     */
    public function getBirthday()
    {
        return $this->getField('birthday');
    }

    /**
     * Returns the current location of the user as a WoarPage.
     *
     * @return WoarPage|null
     */
    public function getLocation()
    {
        return $this->getField('location');
    }

    /**
     * Returns the current location of the user as a WoarPage.
     *
     * @return WoarPage|null
     */
    public function getHometown()
    {
        return $this->getField('hometown');
    }

    /**
     * Returns the current location of the user as a WoarUser.
     *
     * @return WoarUser|null
     */
    public function getSignificantOther()
    {
        return $this->getField('significant_other');
    }

    /**
     * Returns the picture of the user as a WoarPicture
     *
     * @return WoarPicture|null
     */
    public function getPicture()
    {
        return $this->getField('picture');
    }
}
